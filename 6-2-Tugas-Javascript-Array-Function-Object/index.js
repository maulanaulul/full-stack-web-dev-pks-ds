// soal 1
// buatlah variabel seperti di bawah ini

// var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):

// 1. Tokek
// 2. Komodo
// 3. Cicak
// 4. Ular
// 5. Buaya

// Jawaban Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var sorted = daftarHewan.sort()


console.log(sorted.join("\n"))

// soal 2
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]!"

/* 
    Tulis kode function di sini
*/

// Jawaban soal 2
function introduce(input){
    return "Nama Saya "+input.name+", umur saya "+input.age+" tahun, alamat saya di "+input.address+", dan saya punya hobby yaitu "+input.hobby
}
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming" 


// soal 3
// Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.

var hurufVokal = ["a", "i", "u", "e", "o"]


function hitung_huruf_vokal(input){
    var jumlahVokal = 0
    
    var inputArray = input.toLowerCase().split('')
    for(let x=0; x<inputArray.length-1; x++){
        for (let y=0; y<hurufVokal.length-1; y++){
        if(inputArray[x]==hurufVokal[y]){
            jumlahVokal ++
        }
        }
    }
    // console.log(inputArray)
    return jumlahVokal
}


var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2

// soal 4

// Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.

function hitung(number){
    return ((number * 2)-2)
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8


