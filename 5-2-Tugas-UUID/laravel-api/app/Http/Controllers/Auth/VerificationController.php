<?php

namespace App\Http\Controllers\Auth;

use App\Users;
use App\OtpCode;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
		'otp' => 'required'
	])

	if($validator->fails()){
		return response()->json($validator->errors(), 400);}

	$otpCode->delete();
	return response()->json([
            'success' => true, 
            'message' => 'Berhasil mendaftar',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ])
;


    }
}
