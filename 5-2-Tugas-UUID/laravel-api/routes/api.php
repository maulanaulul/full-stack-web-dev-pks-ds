<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::apiResource('/post', 'PostController');
Route::apiResource('/roles', 'RolesController');
Route::apiResource('/comments', 'CommentsController');

Route::group([
'prefix' => 'auth',
'namespace' => 'Auth'],
function(){
Route::post('register', 'RegisterController')->name('auth.register');
Route::post('/register-otp-code','RegenerateOtpCodeController')->name('auth.regenerate');
Route::post('/verification','VerificationController')->name('auth.verification');
Route::post('/update-password','UpdatePasswordController')->name('auth.update_password');
Route::post('/login','LoginController')->name('auth.login');

});
